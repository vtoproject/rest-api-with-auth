<?php
class LogModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getLog($id = null){
			if($id === null){
				return $this->db->get('log');
			} else {
				return $this->db->get_where('log',['id_log'=>$id]);
			}
	}

	public function deleteLog($id) {
			$this->db->delete('log', ['id_log' => $id]);
			return $this->db->affected_rows();
	}

	public function createLog($data) {
			$this->db->insert('log', $data);
			return $this->db->affected_rows();
	} 

	public function updateLog($data, $id) {
			$this->db->update('log', $data, ['id_log' => $id]);
			return $this->db->affected_rows();
	}		
}
?>
<?php
class SnModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_rf(){
        return $this->db->get('ms_snumber');
    } 

    function get_rf_id($id){
        $this->db->get_where('no_kartu',$id);
        return $this->db->get('ms_snumber');
    }

    function add_rf($rf){
        $this->db->insert('ms_snumber',$rf);
        return $this->db->insert_id();
    }
}
?>
<?php
class LogModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function get_all_log($id = null){
			if($id === null){
				return $this->db->get('log');
			} else {
				return $this->db->get_where('log',['id_log'=>$id]);
			}
	}

    public function get_log_id($id){
        $this->db->get_where('id_log',$id);
        return $this->db->get('log');        
    }

    public function add_log($log){
        $this->db->insert('log',$log);
        return $this->db->insert_id();
    }
}
?>
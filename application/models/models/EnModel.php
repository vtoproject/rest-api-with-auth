<?php
class EnModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_rf(){
        return $this->db->get('ms_enumber');
    } 

    function get_rf_id($id){
        $this->db->get_where('id',$id);
        return $this->db->get('ms_enumber');        
    }

    function add_rf($rf){
        $this->db->insert('ms_enumber',$rf);
        return $this->db->insert_id();
    }
}
?>
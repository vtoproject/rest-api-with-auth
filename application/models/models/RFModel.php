<?php
class RFModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all_rf(){
        return $this->db->get('trx_erp');
    } 

    function get_rf_id($id){
        $this->db->get_where('id',$id);
        return $this->db->get('trx_erp');        
    }

    function add_rf($rf){
        $this->db->insert('tr_erp',$rf);
        return $this->db->insert_id();
    }
}
?>
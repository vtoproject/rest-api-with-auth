<?php
class RFModel extends CI_Model
{
	function __construct()
	{
			parent::__construct();
	}

	public function getData($id = null){
		if($id === null){
			return $this->db->get('vallrf');
		} else {
			return $this->db->get_where('vallrf',['id_trx'=>$id]);
		}
	}

	public function deleteData($id) {
			$this->db->delete('trx_erp', ['id_trx' => $id]);
			return $this->db->affected_rows();
	}

	public function createData($data) {
			$this->db->insert('trx_erp', $data);
			return $this->db->affected_rows();
	} 

	public function updateData($data, $id) {
			$this->db->update('trx_erp', $data, ['id_trx' => $id]);
			return $this->db->affected_rows();
	}		
}
?>